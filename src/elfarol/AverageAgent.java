/*
*Keegan Shudy
*
*
*unused and will not be implemented as this is unnecessary
 */
package elfarol;

import static elfarol.ParameterWrapper.*;

import java.util.ArrayList;
import java.util.List;

import elfarol.strategies.AStrategy;
import elfarol.strategies.AverageStrategy;


public class AverageAgent {


	private final List<AStrategy> strategies = new ArrayList<AStrategy>();

	private AStrategy bestStrategy = null;

	private boolean attend = false;


	public AverageAgent() {
		for (int i = 0, n = getStrategiesNumber(); i < n; ++i) {
			strategies.add(new AverageStrategy());
		}

		bestStrategy = strategies.get(0); // Choose the first one initially
		
		updateBestStrategy();
	}

	public boolean isAttending() {
		return attend;
	}


	private double score(final AStrategy strategy) {
		if (null == strategy) {
			throw new IllegalArgumentException("strategy == null");
		}

		double ret = 0.0;
		for (int i = 0; i < getMemorySize(); ++i) {
			final int week = i + 1;
			final double currentAttendance = History.getInstance()
					.getAttendance(i);
			final double prediction = predictAttendance(strategy, History
					.getInstance().getSubHistory(week));

			ret += Math.abs(currentAttendance - prediction);
		}

		assert (ret >= 0);
		return ret;
	}


	private double predictAttendance(final AStrategy strategy,
			final List<Integer> subhistory) {
		assert (strategy.size() - 1 == subhistory.size());

		// Last one is considered with a weight of 1.0
		double ret = strategy.getWeight(0);

		// Start from the second one (where index is 1)
		for (int i = 1; i < strategy.size(); ++i) {
			ret += strategy.getWeight(i) * subhistory.get(i - 1);
		}

		return ret;
	}

	// ========================================================================
	// === Public Interface ===================================================

	/**
	 * Makes the agent evaluate all the strategies and if any of them is better
	 * than the previously used one it is updated. A threshold level of
	 * <code>memorySize * agentsNumber + 1</code> is also considered.
	 */
	public void updateBestStrategy() {
		// Defined threshold level
		double minScore = getMemorySize() * getAgentsNumber() + 1;

		for (final AStrategy strategy : strategies) {
			final double score = score(strategy);
			if (score < minScore) {
				minScore = score;
				bestStrategy = strategy;
			}
		}
		System.out.println("Best strategy "+bestStrategy);
	}

	/**
	 * Makes the agent update its attendance level based on its attendance
	 * prediction by its best evaluated strategy.
	 */
	public void updateAttendance() {
		final double prediction = predictAttendance(bestStrategy, History
				.getInstance().getMemoryBoundedSubHistory());

		attend = (prediction <= getOvercrowdingThreshold());
	}

}
